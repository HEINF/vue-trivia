import { BASE_URL_APP } from ".";

// Queries API for list of users matching username. If none is found, returns false, which will 
// trigger a call to 'apiPostUser' through store, which will create a user with the username.
// If one ore more matches are found, returns the first match
// The api does not check for duplicates. Duplicate checking and a login feature would be suggestions for
// future implementations
export async function apiUserNewOrExist(username) {
    try {
        const response = await fetch(`https://heinf-noroff-assignment-api.herokuapp.com/trivia?username=${username}`)

        const userMatch = await response.json()
        console.log("userMatch", userMatch)
        if (!userMatch.length ) {
            console.log("returning false")
            return false
        }
        return userMatch[0]

    } catch (error) {
        console.log(error)
    }
}

//  Sends a POST request to the API containing username and score of 0 (new user = no previous score)
export async function apiPostUser(username) {
    try {
        const config = {
            method: "POST",
            headers: {
                "X-API-Key": "7b01ccc2-8a1f-4840-bb99-d9ab213f88d2",
                "content-type": "application/json"
            },
            body: JSON.stringify({
                "username": username,
                "highScore": "0"
                
            })
        }

        const response = await fetch("https://heinf-noroff-assignment-api.herokuapp.com/trivia", config)
        const user = await response.json()
        return user

    } catch (error) {
        console.log(error)
    }
}
import { BASE_URL_TRIVIA } from ".";

export async function apiGetQuestions(options) {
  try {
    // hardcoded for testing, need to set up input parameters
    const response = await fetch(
      `${BASE_URL_TRIVIA}amount=${options.amount}&category=${options.category}&difficulty=${options.difficulty}`
    );

    const questionAll = await response.json();
    console.log("This is the questions call", questionAll.results[0].question);
    return questionAll.results;
  } catch (error) {
    console.log(error);
    return false;
  }
}

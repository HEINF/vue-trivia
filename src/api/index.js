// Base URL for the Noroff Assignment API
export const BASE_URL_APP =
  "https://heinf-noroff-assignment-api.herokuapp.com/";
// Base URL to Open Trivia Database API
export const BASE_URL_TRIVIA = "https://opentdb.com/api.php?";
